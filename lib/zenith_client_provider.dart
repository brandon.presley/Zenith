import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:path_provider/path_provider.dart';

class ZenithClientProvider extends ChangeNotifier {
  final client = Client("zenith", databaseBuilder: (_) async {
    final dir =
        await getApplicationSupportDirectory(); // Recommend path_provider package
    final db = HiveCollectionsDatabase('matrix_example_chat', dir.path);
    await db.open();
    return db;
  });
}
