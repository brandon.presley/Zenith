import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zenith/login.dart';
import 'package:zenith/room.dart';
import 'package:zenith/zenith_client_provider.dart';

class RoomsPage extends StatefulWidget {
  const RoomsPage({super.key});

  @override
  State<RoomsPage> createState() => _RoomsPageState();
}

class _RoomsPageState extends State<RoomsPage> {
  bool loadingRooms = true;

  @override
  void initState() {
    super.initState();
  }

  void sendMessage() {}

  void viewRoom(Room room) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => RoomPage(room: room)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text("Zenith"),
        actions: [
          IconButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text("Log out?"),
                    actions: [
                      ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text("Cancel")),
                      ElevatedButton(
                        child: const Text("Confirm"),
                        onPressed: () async {
                          print("Logging out...");
                          final prefs = await SharedPreferences.getInstance();
                          prefs.remove("homeserver");
                          prefs.remove("username");
                          prefs.remove("password");

                          if (!mounted) return;
                          final provider = Provider.of<ZenithClientProvider>(
                              context,
                              listen: false);
                          await provider.client.logout();

                          if (!mounted) return;
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()),
                              (route) => false);
                        },
                      )
                    ],
                  ),
                );
              },
              icon: const Icon(Icons.logout))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(child: Consumer<ZenithClientProvider>(
          builder: (context, provider, child) {
            provider.client.onRoomState.stream.listen((event) {
              setState(() {
                loadingRooms = false;
              });
            });

            if (provider.client.rooms.isEmpty)
              return const CircularProgressIndicator();
            else
              return ListView.builder(
                  itemCount: provider.client.rooms.length,
                  itemBuilder: (context, index) => ListTile(
                        title: Text(provider.client.rooms[index]
                            .getLocalizedDisplayname()
                            .replaceFirst(RegExp("Group with "), "")),
                        leading: CircleAvatar(
                            foregroundImage: NetworkImage(provider
                                    .client.rooms[index].avatar
                                    ?.getThumbnail(provider.client,
                                        width: 50, height: 50)
                                    .toString() ??
                                "")),
                        onTap: () => viewRoom(provider.client.rooms[index]),
                      ));
          },
        )),
      ),
    );
  }
}
