import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zenith/rooms.dart';
import 'package:zenith/zenith_client_provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final serverController = TextEditingController();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  bool loggingIn = false;
  String failedMessage = '';

  void login() async {
    setState(() {
      loggingIn = true;
    });

    final provider = Provider.of<ZenithClientProvider>(context, listen: false);

    try {
      await provider.client.checkHomeserver(Uri.parse(serverController.text));
      await provider.client.login(
        LoginType.mLoginPassword,
        password: passwordController.text,
        identifier: AuthenticationUserIdentifier(user: usernameController.text),
        refreshToken: true,
      );
    } catch (error) {
      print(error);
      setState(() {
        failedMessage = error.toString();
        loggingIn = false;
      });

      Future.delayed(const Duration(seconds: 10), () {
        setState(() {
          failedMessage = '';
        });
      });
      return;
    } finally {
      setState(() {
        loggingIn = false;
      });
    }

    final prefs = await SharedPreferences.getInstance();
    prefs.setString("homeserver", serverController.text);
    prefs.setString("username", usernameController.text);
    prefs.setString("password", passwordController.text);

    if (!mounted) return;
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => const RoomsPage()),
        (route) => false);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ZenithClientProvider>(
      builder: (context, provider, child) => Scaffold(
          appBar: AppBar(
            backgroundColor: Theme.of(context).colorScheme.inversePrimary,
            title: const Text("Login"),
          ),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                    controller: serverController,
                    autocorrect: false,
                    textCapitalization: TextCapitalization.none,
                    decoration: const InputDecoration(
                        labelText: 'Server', hintText: 'https://matrix.org'),
                  ),
                  TextFormField(
                    controller: usernameController,
                    autocorrect: false,
                    decoration: const InputDecoration(
                        labelText: 'Username', hintText: 'john'),
                  ),
                  TextFormField(
                    controller: passwordController,
                    autocorrect: false,
                    decoration: const InputDecoration(labelText: 'Password'),
                    obscureText: true,
                    onFieldSubmitted: (value) => login(),
                  ),
                  Text(failedMessage,
                      style: Theme.of(context).textTheme.headlineSmall)
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: loggingIn ? null : login,
            tooltip: 'Log in',
            child: loggingIn
                ? const CircularProgressIndicator()
                : const Icon(Icons.login),
          )),
    );
  }
}
