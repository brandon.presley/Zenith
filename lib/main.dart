import 'package:flutter/material.dart';
import 'package:matrix/matrix.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:zenith/rooms.dart';
import 'package:zenith/login.dart';
import 'package:provider/provider.dart';
import 'package:zenith/zenith_client_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ZenithClientProvider(),
      child: MaterialApp(
        title: 'Zenith',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool showRooms = false;
  String errorMessage = "";

  void getCreds() async {
    final prefs = await SharedPreferences.getInstance();
    final homeserver = prefs.getString("homeserver");
    final username = prefs.getString("username");
    final password = prefs.getString("password");

    if (homeserver == null || username == null || password == null) return;

    if (!mounted) return;
    final provider = Provider.of<ZenithClientProvider>(context, listen: false);
    setState(() {
      showRooms = true;
    });

    try {
      await provider.client.checkHomeserver(Uri.parse(homeserver));
      if (!provider.client.isLogged())
        await provider.client.login(LoginType.mLoginPassword,
            password: password,
            identifier: AuthenticationUserIdentifier(user: username));
    } catch (error) {
      print(error);
      setState(() {
        errorMessage = error.toString();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getCreds();
  }

  @override
  Widget build(BuildContext context) {
    if (errorMessage.isNotEmpty)
      return Column(
        children: [
          ElevatedButton(
              onPressed: () {
                setState(() {
                  errorMessage = "";
                });
              },
              child: const Text("Clear")),
          ErrorWidget(errorMessage),
        ],
      );
    if (showRooms)
      return const RoomsPage();
    else
      return const LoginPage();
  }
}
